/*
 * Copyright  2012 Davy Preuveneers. All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt).
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <QWidget>
#include <QLabel>
#include <QKeyEvent>
#include <QMutex>
#include <QMutexLocker>
#include <QMap>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void resetFrameBufferSize(int w, int h);
void keyEvent(unsigned ch);
void mouseEvent(int x, int y, int pressed);
void enableStateChange();
bool isRotated();

typedef QMap<unsigned short *,bool> Buffer;

class FrameBuffer : public QWidget
{
    Q_OBJECT

public:
    FrameBuffer(QWidget *parent = 0, int w = 480, int h = 640/*, unsigned short *ptr = 0*/);
    void update(unsigned short * surface_buffer = (unsigned short *)0);
    void toggleFullScreen();
    Buffer buffer;
    
    void repaintPixmap(unsigned short * surface_buffer);
public slots:
    void repaintPixmap(){repaintPixmap(0);}

signals:
    void refresh();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    int width;
    int height;
    unsigned short *pixeldata;
};

#endif
