/*
 * Copyright  2012 Davy Preuveneers. All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt).
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <QtGui>
#include <QMatrix>
#include <QMutexLocker>

#include "framebuffer.h"

FrameBuffer::FrameBuffer(QWidget *parent, int w, int h/*, unsigned short *ptr*/) : 
  QWidget(parent),
  width(w),
  height(h),
  pixeldata(0)
{
#ifdef Q_WS_MAEMO_5
    setAttribute(Qt::WA_Maemo5AutoOrientation, true);
    setAttribute(Qt::WA_Maemo5NonComposited);
#endif

    setAttribute(Qt::WA_OpaquePaintEvent, true);

    setFixedSize(width, height);
    connect(this, SIGNAL(refresh()), this, SLOT(repaintPixmap()));
}

void FrameBuffer::update(unsigned short * surface_buffer) 
{
  if(surface_buffer)
    pixeldata = surface_buffer;
    emit refresh();
}

void FrameBuffer::toggleFullScreen() {
    enableStateChange();

    bool isFullScreen = windowState() & Qt::WindowFullScreen;
    if (isFullScreen) {
        showNormal();
    } else {
        showFullScreen();
    }
}

void FrameBuffer::repaintPixmap(unsigned short * surface_buffer) {
    /*if (isRotated()) {
        QImage image((uchar *)pixeldata, height, width, QImage::Format_RGB16);
        QPixmap pixmap = QPixmap::fromImage(image);

        QMatrix rm;
        rm.rotate(270);
        setPixmap(pixmap.transformed(rm));
    } else {
        QImage image((uchar *)pixeldata, width, height, QImage::Format_RGB16);
        QPixmap pixmap = QPixmap::fromImage(image);
        setPixmap(QPixmap::fromImage(image));
    }*/
  if(surface_buffer)
    pixeldata = surface_buffer;
  repaint();
}

void FrameBuffer::resizeEvent(QResizeEvent * event) {
    width = event->size().width();
    height = event->size().height();

    resetFrameBufferSize(width, height);
}

void FrameBuffer::keyPressEvent(QKeyEvent *event) {
    unsigned short ch = 0x00;

    QString text = event->text();
    if (text.length() > 0) {
        ch = text.at(0).unicode();
    } else {
        if (event->modifiers() & Qt::ControlModifier) {
            switch (event->key()) {
            case Qt::Key_Up:
                toggleFullScreen();
                break;
            case Qt::Key_Left:
                ch = 0x05;
                break;
            case Qt::Key_Right:
                ch = 0x06;
                break;
            }
        } else {
            switch (event->key()) {
            case Qt::Key_Up:
                ch = 0x01;
                break;
            case Qt::Key_Down:
                ch = 0x02;
                break;
            case Qt::Key_Left:
                ch = 0x03;
                break;
            case Qt::Key_Right:
                ch = 0x04;
                break;
            case Qt::Key_Escape:
                qApp->quit();
                break;
            case Qt::Key_F11:
                toggleFullScreen();
                break;
            }
        }
    }

    keyEvent(ch);
}

void FrameBuffer::mousePressEvent(QMouseEvent* event) {
    int x = event->x();
    int y = event->y();

    mouseEvent(x, y, 1);
}

void FrameBuffer::mouseMoveEvent(QMouseEvent* event) {
    int x = event->x();
    int y = event->y();

    mouseEvent(x, y, 1);
}

void FrameBuffer::mouseReleaseEvent(QMouseEvent* event) {
    int x = event->x();
    int y = event->y();

    mouseEvent(x, y, 0);
}

void FrameBuffer::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);
  if(pixeldata)
  {
    QImage image((uchar *)pixeldata, width, height, QImage::Format_RGB16);
    painter.drawImage(geometry(),image,geometry());
    buffer[pixeldata] = false;
  }
}
