/*
 * Copyright  2012 Davy Preuveneers. All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt).
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <QApplication>
#include <QtGui>
#include <QtCore>
#include <QMediaPlayer>
#include <QDebug>

#ifdef Q_WS_MAEMO_5_OLD
#include <location/location-gps-device.h>
#include <location/location-gpsd-control.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <execinfo.h>

#include "framebuffer.h"
#include "locationwidget.h"
#include "cvmthread.h"
#include "main.h"

#define MOUSE_FD "/tmp/mouse_fd"
#define KEYBOARD_FD "/tmp/keyboard_fd"

#define SURFACE_WIDTH       854
#define SURFACE_HEIGHT      480 // 424

#define STYLE_PLAIN         0
#define STYLE_BOLD          1
#define STYLE_ITALIC        2
#define STYLE_UNDERLINED    4

#define SIZE_SMALL          8
#define SIZE_MEDIUM         0
#define SIZE_LARGE         16

#define FACE_SYSTEM         0
#define FACE_MONOSPACE     32
#define FACE_PROPORTIONAL  64

typedef unsigned short gxj_pixel_type;
typedef unsigned char  gxj_alpha_type;

typedef struct _gxj_screen_buffer {
    int width;
    int height;
    gxj_pixel_type *pixelData;
    gxj_alpha_type *alphaData;
} gxj_screen_buffer;

static int width = 800;            // N900: 800, N9: 854
static int height = 480;           // N900: 424, N9: 376
static int midlet_argc;
static char **midlet_argv;

static FrameBuffer *fb = NULL;
static int input_ready = 0;
static int enable_state_change = 0;

static FILE* keyboard_fd = NULL;
static FILE* mouse_fd = NULL;

static jboolean *valid = NULL;
static jdouble *latitude = NULL;
static jdouble *longitude = NULL;
static jfloat *altitude = NULL;
static jfloat *speed = NULL;
static jfloat *bearing = NULL;
static jfloat *accuracy = NULL;

#ifdef Q_WS_MAEMO_5_OLD
static LocationGPSDevice *device = NULL;
static LocationGPSDControl *control = NULL;
#endif

static LocationWidget *location = NULL;
static QMediaPlayer *player = NULL;

static CVMThread thread;

static bool fullscreen = true;
static bool rotate = false;
static bool antialias = true;
static bool bitmapfont = false;
static bool errorlog = false;
static bool sip = false;

void update_location(bool val, double lat, double lon, float alt, float spd, float brg, float acc) {
    *valid = val;
    *latitude = lat;
    *longitude = lon;
    *altitude = alt;
    *speed = spd;
    *bearing = brg;
    *accuracy = acc;
}

void enableStateChange() {
    enable_state_change = 1;
}

static void repaintPixmap(void *src) {
    if (fb && src) 
    {
      static gxj_pixel_type * last_buffer = 0;
      
      if(last_buffer && fb->buffer.contains(last_buffer))
        while(fb->buffer[last_buffer])
          usleep(1);
      
      last_buffer = (unsigned short *)src;
      
      fb->buffer[last_buffer] = true;
      
      fb->update(last_buffer);
    }

    enable_state_change = 1;
    input_ready = 1;
}

void cvm_exec() {
    cvm_main(midlet_argc, midlet_argv);
}

bool isRotated() {
    return rotate;
}

void keyEvent(unsigned int ch) {
    if (!input_ready)
        return;

    if (!keyboard_fd) {
        char buff[255];
        sprintf(buff, "/tmp/keyboard_fd.%d", getpid());
        keyboard_fd = fopen(buff, "w");
    }

    if (!keyboard_fd)
        return;

    if (!ch)
        return;

    fputc(ch, keyboard_fd);
    fputc(ch/256, keyboard_fd);

    fputc(0x80, keyboard_fd);
    fputc(0x00, keyboard_fd);

    fputc(ch, keyboard_fd);
    fputc(ch/256, keyboard_fd);
    fflush(keyboard_fd);
}

void mouseEvent(int x, int y, int pressed) {
    if (!input_ready)
        return;

    if (!mouse_fd) {
        char buff[255];
        sprintf(buff, "/tmp/mouse_fd.%d", getpid());
        mouse_fd = fopen(buff, "w");
    }

    if (!mouse_fd)
        return;

    if (rotate) {
        int tmp = x;
        x = height - y;
        y = tmp;
    }

    fputc(x & 0xff, mouse_fd);
    x >>= 8;
    fputc(x & 0xff, mouse_fd);
    x >>= 8;
    fputc(x & 0xff, mouse_fd);
    x >>= 8;
    fputc(x & 0xff, mouse_fd);

    fputc(y & 0xff, mouse_fd);
    y >>= 8;
    fputc(y & 0xff, mouse_fd);
    y >>= 8;
    fputc(y & 0xff, mouse_fd);
    y >>= 8;
    fputc(y & 0xff, mouse_fd);

    fputc(pressed, mouse_fd);
    fputc(0, mouse_fd);
    fputc(0, mouse_fd);
    fputc(0, mouse_fd);

    fflush(mouse_fd);
}

void resetFrameBufferSize(int w, int h) {

    width = w;
    height = h;

    if (rotate) {
        resizeFrameBuffer(height, width);
    } else {
        resizeFrameBuffer(width, height);
    }

    if (enable_state_change) {
        keyEvent(0x15);
        if (fb) {
            fb->update();
        }
    }

    enable_state_change = 1; // Force a size reset the next time this method is called
}

static int gxjport_get_chars_width(int face, int style, int size,
                                   const jchar *charArray, int n) {

    QFont font = QApplication::font();
    font.setPixelSize(size);
    if (style & STYLE_BOLD) {
        font.setWeight(QFont::Bold);
    }
    if (style & STYLE_ITALIC) {
        font.setItalic(true);
    }
    if (style & STYLE_UNDERLINED) {
        font.setUnderline(true);
    }

    QFontMetrics fm(font);
    QString s((QChar *)charArray, n);

    return fm.width(s);
}

static int gxjport_draw_chars(int pixel, const jshort *clip, void *dst, int dotted,
                              int face, int style, int size,
                              int x, int y, int anchor,
                              const jchar *charArray, int n) {

    gxj_screen_buffer *dest = (gxj_screen_buffer *)dst;

    QFont font = QApplication::font();
    if (!antialias) {
        font.setStyleStrategy(QFont::NoAntialias);
    }
    font.setPixelSize(size);
    if (style & STYLE_BOLD) {
        font.setWeight(QFont::Bold);
    }
    if (style & STYLE_ITALIC) {
        font.setItalic(true);
    }
    if (style & STYLE_UNDERLINED) {
        font.setUnderline(true);
    }

    QFontMetrics fm(font);
    QString s((QChar *)charArray, n);

    int font_width = fm.width(s);
    int font_height = fm.height();

    QImage image((uchar *)dest->pixelData, dest->width, dest->height, QImage::Format_RGB16);
    QPainter painter(&image);
    painter.setClipping(true);
    painter.setFont(font);
    painter.setPen(QColor(pixel));

    QRect rect = QRect(clip[0], clip[1], clip[2]-clip[0], clip[3]-clip[1]);
    painter.setClipRect(rect);

    painter.drawText(x, y, font_width, font_height, Qt::AlignLeft|Qt::AlignTop, s);

    return 1;
}

static int gxjport_get_font_info(int face, int style, int size,
                                 int *ascent, int *descent, int *leading) {

    QFont font = QApplication::font();
    font.setPixelSize(size);
    if (style & STYLE_BOLD) {
        font.setWeight(QFont::Bold);
    }
    if (style & STYLE_ITALIC) {
        font.setItalic(true);
    }
    if (style & STYLE_UNDERLINED) {
        font.setUnderline(true);
    }

    QFontMetrics fm(font);
    *ascent = fm.ascent();
    *descent = fm.descent();
    *leading = fm.leading();

    return 1;
}

#ifdef Q_WS_MAEMO_5_OLD
void location_changed(LocationGPSDevice *device, gpointer userdata) {
    (void)userdata;

    if (device->status == LOCATION_GPS_DEVICE_STATUS_NO_FIX) {
        // fprintf(stderr, "No Fix\n");
        *valid = false;
    } else {
        // fprintf(stderr, "Fixed\n");
        *valid = true;
    }

    // fprintf(stderr, "Satellites in use: %d\n", device->satellites_in_use);
    LocationGPSDeviceFix *fix = device->fix;

    if ((fix->mode == LOCATION_GPS_DEVICE_MODE_3D) && (fix->fields & LOCATION_GPS_DEVICE_ALTITUDE_SET)) {
        // fprintf(stderr, "Altitude: %f m\n", fix->altitude);
        *altitude = (jfloat)(fix->altitude);
    }

    if ((fix->mode == LOCATION_GPS_DEVICE_MODE_3D || fix->mode == LOCATION_GPS_DEVICE_MODE_2D) && (fix->fields & LOCATION_GPS_DEVICE_LATLONG_SET)) {
        // fprintf(stderr, "Latitude: %f - Longitude: %f\n", fix->latitude, fix->longitude);

        *latitude = fix->latitude;
        *longitude = fix->longitude;
    }

    if (fix->fields & LOCATION_GPS_DEVICE_SPEED_SET) {
        // fprintf(stderr, "Speed: %f km/h\n", fix->speed);
        *speed = (jfloat)(fix->speed);
    }

    // fprintf(stderr, "Timestamp: %f s\n", fix->time);

    // fprintf(stderr, "Horizontal accuracy: %f m\n", fix->eph/100);
    *accuracy = (jfloat)(fix->eph/100);

    // fprintf(stderr, "Bearing: %f degrees\n", fix->track);
    *bearing = (jfloat)(fix->track);
}

void location_connected(LocationGPSDevice *device, gpointer userdata) {
    (void)device;
    (void)userdata;
}

void location_disconnected(LocationGPSDevice *device, gpointer userdata) {
    (void)device;
    (void)userdata;
}
#endif

void init_gps_provider(jboolean *v, jdouble *lat, jdouble *lon, jfloat *alt, jfloat *s, jfloat *b, jfloat *a) {
    valid = v;
    latitude = lat;
    longitude = lon;
    altitude = alt;
    speed = s;
    bearing = b;
    accuracy = a;

#ifdef Q_WS_MAEMO_5_OLD
    control = location_gpsd_control_get_default();
    if (control)
        location_gpsd_control_start(control);

    device = (LocationGPSDevice*)g_object_new(LOCATION_TYPE_GPS_DEVICE, NULL);
    if (device) {
        g_signal_connect(device, "changed", G_CALLBACK(location_changed), NULL);
        g_signal_connect(device, "connected", G_CALLBACK(location_connected), NULL);
        g_signal_connect(device, "disconnected", G_CALLBACK(location_disconnected), NULL);
    }
#endif

    if (location) {
        location->startUpdates();
    }
}

void finish_gps_provider() {
#ifdef Q_WS_MAEMO_5_OLD
    if (device) {
        g_object_unref(device);
        device = NULL;
    }

    if (control) {
        location_gpsd_control_stop(control);
        control = NULL;
    }
#endif

    if (location) {
        location->stopUpdates();
    }
}

bool checkparam(int &argc, char *argv[], char *param) {
    for (int i=0; i<argc; i++) {
        if (!strcmp(argv[i], param)) {
            for (int j=i+1; j<argc; j++) {
                argv[j-1] = argv[j];
            }
            argc--;
            return true;
        }
    }
    return false;
}

static void cleanup() {
    char buff[255];

    sprintf(buff, "%s.%d", KEYBOARD_FD, getpid());
    remove(buff);

    sprintf(buff, "%s.%d", MOUSE_FD, getpid());
    remove(buff);
}

void segvhandler(int signum) {
    fprintf(stderr, "ERROR: Catched signal %d\n", signum);

    void *array[16];
    size_t size;

    size = backtrace(array, 16);
    backtrace_symbols_fd(array, size, 2);

    finish_gps_provider();
    exit(1);
}

void inthandler(int sig) {
    signal(sig, SIG_IGN);
    /// finalizeFrameBuffer();

    finish_gps_provider();
    exit(0);
}

void playFile(QString filename) {
    player->setMedia(QUrl::fromLocalFile(filename));
    player->play();
}

void playAlert(int i) {
#ifdef Q_WS_MAEMO_5
    if (i == 3) // Error
        playFile(QString("/usr/share/sounds/ui-default_beep.wav"));
    else if (i == 2) // Warning
        playFile(QString("/usr/share/sounds/ui-general_warning.wav"));
    else if (i == 1) // Info - Command
        playFile(QString("/usr/share/sounds/ui-information_note.wav"));
    else if (i == 5) // Confirmation
        playFile(QString("/usr/share/sounds/ui-confirmation_note.wav"));
    else if (i == 4) // Alarm
        playFile(QString("/usr/share/sounds/ui-battery_low.wav"));
    else
        playFile(QString("/usr/share/sounds/ui-default_beep.wav"));
#else
    if (i == 3)
        playFile(QString("/usr/share/sounds/ui-tones/snd_default_beep.wav"));
    else if (i == 2)
        playFile(QString("/usr/share/sounds/ui-tones/snd_warning.wav"));
    else if (i == 1)
        playFile(QString("/usr/share/sounds/ui-tones/snd_information.wav"));
    else if (i == 5)
        playFile(QString("/usr/share/sounds/ui-tones/snd_query.wav"));
    else if (i == 4)
        playFile(QString("/usr/share/sounds/ui-tones/snd_warning_strong.wav"));
    else
        playFile(QString("/usr/share/sounds/ui-tones/snd_default_beep.wav"));
#endif
}

int call_platform(char *invocation[16], int count, char *result[16]) {
    int i, ret;

    fprintf(stderr, "INFO:");
    for (i=0; i<count; i++) {
        fprintf(stderr, " %s", invocation[i]);
    }
    fprintf(stderr, "\n");

    strcpy(result[0], "");
    ret = 1;

    if (count >= 3) {
        if (strcmp(invocation[0], "maemo.media.MediaPlayer") == 0) {
            if (strcmp(invocation[1], "playAlert") == 0) {
                playAlert(atoi(invocation[2]));
                ret = 0;
            }
            if (strcmp(invocation[1], "create") == 0) {
                player->setMedia(QUrl::fromLocalFile(invocation[2]));
                ret = 0;
            }
            if (strcmp(invocation[1], "start") == 0) {
                player->play();
                ret = 0;
            }
            if (strcmp(invocation[1], "stop") == 0) {
                player->stop();
                ret = 0;
            }
        }
    }

    return ret;
}

int main(int argc, char *argv[]) {
    char *ptr;
    char path[256];
    void *handle;
    int gui = 0;

    if (atexit(cleanup)) {
        fprintf(stderr, "ERROR: Failed to register callback.\n");
    }

    struct sigaction sigact;
    signal(SIGINT, inthandler);

    // set up new handler to specify new action
    sigact.sa_handler = segvhandler;
    // sigemptyset (&sigact.sa_mask);
    sigact.sa_flags = 0;
    // attach SIGSEV to segvhandler
    sigaction(SIGSEGV, &sigact, NULL);

    QSettings settings("phoneme", "user");
    fullscreen = settings.value("fullscreen", true).toBool();
    rotate = settings.value("rotate", false).toBool();
    antialias = settings.value("antialias", true).toBool();
    bitmapfont = settings.value("bitmapfont", false).toBool();
    errorlog = settings.value("errorlog", false).toBool();
    sip = settings.value("sip", false).toBool();

    if (errorlog) {
        freopen("/home/user/MyDocs/out.txt", "w", stdout);
        freopen("/home/user/MyDocs/err.txt", "w", stderr);
    }

    for (int i=0; i<argc; i++) {
        if (!strcmp(argv[i], "sun.misc.MIDPLauncher")) {
            gui = 1;
            break;
        }
    }

    if (checkparam(argc, argv, (char *)"-fullscreen")) {
        fullscreen = true;
    }

    if (checkparam(argc, argv, (char *)"-nofullscreen")) {
        fullscreen = false;
    }

    if (checkparam(argc, argv, (char *)"-rotate")) {
        rotate = true;
    }

    if (checkparam(argc, argv, (char *)"-norotate")) {
        rotate = false;
    }

    if (checkparam(argc, argv, (char *)"-bitmapfont")) {
        bitmapfont = true;
    }

    if (checkparam(argc, argv, (char *)"-nobitmapfont")) {
        bitmapfont = false;
    }

    if (checkparam(argc, argv, (char *)"-antialias")) {
        antialias = true;
    }

    if (checkparam(argc, argv, (char *)"-noantialias")) {
        antialias = false;
    }

    midlet_argc = argc;
    midlet_argv = argv;

    if (rotate) {
        initPixmap(height, width, &repaintPixmap);
    } else {
        initPixmap(width, height, &repaintPixmap);
    }

    if (!bitmapfont)
        initDrawChars(&gxjport_draw_chars, &gxjport_get_font_info, &gxjport_get_chars_width);

    initPlatform(&call_platform);

    //initGPS(&init_gps_provider, &finish_gps_provider);

    if (gui) {
        char buf[255];
        sprintf(buf, "phoneME");

        if (argc > 1) {
            snprintf(buf, sizeof(buf)-1, "%s", argv[argc-1]);

            char *ptr = strrchr(argv[argc-1], '.');
            if (ptr)
                snprintf(buf, sizeof(buf)-1, "%s", ptr+1);
        }

        FILE *f = fopen("/etc/issue", "r");
        char issue[256];
        fgets(issue, 255, f);
        fclose(f);

        if (strncmp(issue, "Maemo 5", 7) == 0) {
            if (fullscreen) {
                width = 800;
                height = 480;
            } else {
                width = 800;
                height = 424;
            }
        } else if (strncmp(issue, "MeeGo", 5) == 0) {
            width = 854;
            height = 480;
        } else {
            width = 854;
            height = 480;
        }

#ifdef Q_WS_MAEMO_5_OLD
        g_type_init();
#endif

        QApplication app(argc, argv);
        app.setApplicationName("phoneME");
        app.setProperty("NoMStyle", true);
        FrameBuffer window(0, width, height);
        window.setWindowTitle(buf);

        if (fullscreen) {
            window.showFullScreen();
        } else {
            window.show();
        }
        thread.start();

        fb = &window;


        player = new QMediaPlayer(0, QMediaPlayer::LowLatency);
        player->setVolume(85);

        location = new LocationWidget(fb);

        app.exec();

        if (thread.isRunning()) {
            thread.quit();
        }

        delete player;
        delete location;

        finish_gps_provider();

        /// (*fn_finalizeFrameBuffer)();
    } else {
        cvm_exec();
    }

    return 0;
}
