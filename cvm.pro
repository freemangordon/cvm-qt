TARGET      = cvm
HEADERS     += framebuffer.h cvmthread.h locationwidget.h main.h
SOURCES     += framebuffer.cpp cvmthread.cpp locationwidget.cpp main.cpp
FORMS       += #UIS#
LEXSOURCES  += #LEXS#
YACCSOURCES += #YACCS#

INCLUDEPATH +=
LIBS        += -Llib -lcvm
DEFINES     += 

#CONFIG      += link_pkgconfig
#PKGCONFIG   += liblocation

CONFIG      += mobility
MOBILITY     = location
MOBILITY    += multimedia

QMAKE_CXXFLAGS    += -rdynamic

# All generated files goes to the same directory
OBJECTS_DIR = build
MOC_DIR     = build
RCC_DIR     = build
UI_DIR      = build

DESTDIR     = build
TEMPLATE    = app
DEPENDPATH  +=
VPATH       += src uis
CONFIG      -= 
CONFIG      += debug
QT          = core gui #phonon

INSTALLS    += target
target.path  = /opt/phoneme/bin

INSTALLS      += desktop
desktop.path   = /usr/share/applications/hildon
desktop.files  = data/runmidlet.desktop
desktop.files += data/midlet.desktop
desktop.files += data/midletsettings.desktop
desktop.files += data/operamini.desktop
desktop.files += data/location.desktop
desktop.files += data/microemu-demo.desktop

INSTALLS   += package
package.path   = /usr/share/mime/packages
package.files  = data/midlet.xml

INSTALLS       += desktop_n9
desktop_n9.path   = /usr/share/applications
desktop_n9.files  = data/runmidlet.desktop
desktop_n9.files += data/midlet.desktop
desktop_n9.files += data/midletsettings.desktop
desktop_n9.files += data/operamini.desktop
desktop_n9.files += data/microemu-demo.desktop

INSTALLS      += service
service.path   = /usr/share/dbus-1/services
service.files  = data/midlet.service

INSTALLS     += icon
icon.path   = /usr/share/icons/hicolor/64x64/apps
icon.files  = data/64x64/duke.png
icon.files += data/64x64/operamini.png
icon.files += data/64x64/microemu-demo.png
icon.files += data/64x64/location.png

INSTALLS     += mimetypes
mimetypes.path   = /usr/share/icons/hicolor/64x64/mimetypes
mimetypes.files  = data/64x64/application-java-archive.png
mimetypes.files += data/64x64/application-x-java-archive.png
mimetypes.files += data/64x64/text-vnd.sun.j2me.app-descriptor.png

INSTALLS     += icon_n9
icon_n9.path   = /usr/share/themes/blanco/meegotouch/icons
icon_n9.files  = data/64x64/duke.png
icon_n9.files += data/64x64/operamini.png
icon_n9.files += data/64x64/microemu-demo.png
icon_n9.files += data/64x64/location.png

INSTALLS     += bin
bin.path   = /opt/phoneme/bin
bin.files  = ../runmidlet/build/runmidlet
bin.files += ../midletservice/build/midletservice
bin.files += ../settings/build/settings

INSTALLS     += lib
lib.path   = /usr/lib
lib.files  = lib/libcvm.so.1

INSTALLS      += phoneme
phoneme.path   = /opt/phoneme
phoneme.files  = ../legal
phoneme.files += ../lib
phoneme.files += ../midp
phoneme.files += ../*.jar
phoneme.files += ../*.jad
phoneme.files += ../*.zip
phoneme.files += ../*.sh

#
# Targets for debian source and binary package creation
#
debian-src.commands = dpkg-buildpackage -S -r -us -uc -d
debian-bin.commands = dpkg-buildpackage -b -r -uc -d
debian-all.depends = debian-src debian-bin

#
# Clean all but Makefile
#
compiler_clean.commands = -$(DEL_FILE) $(TARGET)

QMAKE_EXTRA_TARGETS += debian-all debian-src debian-bin compiler_clean
